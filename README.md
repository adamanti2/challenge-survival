# ChallengeSurvival

A co-op mass multiplayer survival event in which every player is disadvantaged by a unique "challenge". Players must work together to overcome their challenges and beat the game.

Download the data pack: [latest version](https://gitlab.com/adamanti2/challenge-survival/-/releases/permalink/latest)
