from ../challenges import emitToAll, emitToOwn
from ../scores import scores
from bolt_expressions import Data
from nbtlib import Short


def underscore(functionName):
    newFunctionName = "challengesurvival:_" + functionName[functionName.rfind('/'):]
    functionName = functionName.replace("./", "challengesurvival:")
    function newFunctionName:
        function functionName

underscore("./start_selecting")

def placeMarker(markerTag):
    kill @e[type=marker,tag=f"chlg.{markerTag}"]
    summon marker ~ ~ ~ {Tags:[f"chlg.{markerTag}"]}

underscore("./admin/place_spawn")
function ./admin/place_spawn:
    placeMarker("spawn")
    setworldspawn

underscore("./admin/place_limbo")
function ./admin/place_limbo:
    placeMarker("limbo")


underscore("./admin/challenge_clear")
function ./admin/challenge_clear:
    emitToOwn("clear")
    scores.challenge['@s'] = 0
    team leave @s

function ./_/challenge_activate:
    $function challengesurvival:admin/challenge_activate {c: $(c)}
function ./admin/challenge_activate:
    if score @s chlg.challenge matches 1.. run function ./admin/challenge_clear
    $scoreboard players set @s chlg.challenge $(c)
    $team join challenge$(c) @s
    emitToOwn("onSelect")
    emitToOwn("onSpawn")

underscore("./admin/reset_player")
function ./admin/reset_player:
    function ./admin/challenge_clear
    function ./clear_selection_tags
    tag @s remove chlg.rerolled
    clear @s
    tag @s remove chlg.joined_before

underscore("./admin/place_cage_here")
function ./admin/place_cage_here:
    boxSize = 20
    innerHeight = 3
    hOffset = boxSize / 2
    nhOffset = -hOffset
    align xyz positioned ~0.5 ~ ~0.5 run function ./admin/place_limbo
    fill ~f"{nhOffset - 1}" ~-2 ~f"{nhOffset - 1}" ~f"{hOffset + 1}" ~f"{innerHeight + 1}" ~f"{hOffset + 1}" barrier
    fill ~nhOffset ~-1 ~nhOffset ~hOffset ~innerHeight ~hOffset glass hollow
    fill ~nhOffset ~-1 ~nhOffset ~hOffset ~-1 ~hOffset barrier
    fill ~nhOffset ~-1 ~nhOffset ~nhOffset ~-1 ~hOffset magenta_terracotta
    fill ~nhOffset ~-1 ~nhOffset ~hOffset ~-1 ~nhOffset magenta_terracotta
    fill ~hOffset ~-1 ~hOffset ~nhOffset ~-1 ~hOffset magenta_terracotta
    fill ~hOffset ~-1 ~hOffset ~hOffset ~-1 ~nhOffset magenta_terracotta
    fill ~nhOffset ~innerHeight ~nhOffset ~nhOffset ~innerHeight ~hOffset magenta_terracotta
    fill ~nhOffset ~innerHeight ~nhOffset ~hOffset ~innerHeight ~nhOffset magenta_terracotta
    fill ~hOffset ~innerHeight ~hOffset ~nhOffset ~innerHeight ~hOffset magenta_terracotta
    fill ~hOffset ~innerHeight ~hOffset ~hOffset ~innerHeight ~nhOffset magenta_terracotta


function ./utils/drop_stored_item:
    in minecraft:overworld positioned 0.0 0.0 0.0 run summon item ~ ~ ~ {Item:{id:"minecraft:stick",Count:1b},PickupDelay:40s,Tags:[chlg.current],Air:0s}
    anchored eyes positioned ^ ^ ^ as @e[type=item,tag=chlg.current]:
        scores.airToggle['@s'] = 20 * 3
        scores.var['.airToggle'] += 1
        Data.entity('@s')['Item'] = Data.storage('challengesurvival:misc')['Current']

        anchored feet positioned as @s:
            tp @s ^ ^ ^0.35
        Data.storage('challengesurvival:misc')['Current'] = Data.entity('@s')['Pos']

        tp @s ~ ~ ~
        Data.entity('@s')['Motion'] = Data.storage('challengesurvival:misc')['Current']
        
        Data.entity('@s')['Air'] = Short(1)
        tag @s remove chlg.current

def drop(hand):
    unless predicate f"challengesurvival:{hand}_empty":
        if hand == "mainhand":
            Data.storage('challengesurvival:misc')['Current'] = Data.entity('@s')['SelectedItem']
        elif hand == "offhand":
            Data.storage('challengesurvival:misc')['Current'] = Data.entity('@s')['Inventory']['{Slot:-106b}']
        else:
            raise Exception(f"Unknown hand {hand}")

        function ./utils/drop_stored_item
        
        item replace entity @s f"weapon.{hand}" with air

function ./utils/drop_mainhand:
    drop("mainhand")

function ./utils/drop_offhand:
    drop("offhand")


predicate ./sneaking {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "flags": {
            "is_sneaking": true
        }
    }
}

predicate ./mainhand_empty {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": [ "minecraft:air" ]
            }
        }
    }
}

predicate ./offhand_empty {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "offhand": {
                "items": [ "minecraft:air" ]
            }
        }
    }
}

predicate ./in_nether {
    "condition": "minecraft:location_check",
    "predicate": {
        "dimension": "minecraft:the_nether"
    }
}

predicate ./in_water {
    "condition": "minecraft:all_of",
    "terms": [
        {
            "condition": "minecraft:any_of",
            "terms": [
                {
                    "condition": "minecraft:location_check",
                    "predicate": {
                        "block": {
                            "tag": "challengesurvival:tags/waterloggable",
                            "state": {
                                "waterlogged": "true"
                            }
                        }
                    }
                },
                {
                    "condition": "minecraft:location_check",
                    "predicate": {
                        "block": {
                            "tag": "challengesurvival:tags/water"
                        }
                    }
                }
            ]
        },
        {
            "condition": "minecraft:inverted",
            "term": {
                "condition": "minecraft:reference",
                "name": "challengesurvival:in_boat"
            }
        }
    ]
}

predicate ./in_boat {
    "condition": "minecraft:any_of",
    "terms": [
        {
            "condition": "minecraft:entity_properties",
            "entity": "this",
            "predicate": {
                "vehicle": {
                    "type": "minecraft:boat"
                }
            }
        },
        {
            "condition": "minecraft:entity_properties",
            "entity": "this",
            "predicate": {
                "vehicle": {
                    "type": "minecraft:chest_boat"
                }
            }
        }
    ]
}
