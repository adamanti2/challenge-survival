from bolt_expressions import Data
from adamanti_bolt:utils import vanillaTag, Remember
from ./challenges import challenges, emitToAll, emitToOwn
from ./challenge_ids import IDs
from ./scores import scores, triggers, tickers
import ./tags as _
import ./utils as _
from ./utils_module import escapeDoubleQuotes
from math import floor



function vanillaTag('load', ./load):
    gamerule spawnRadius 0

    scores.init()
    triggers.init()
    scoreboard objectives setdisplay list scores.s_deaths

    for challenge in challenges:
        teamName = f"challenge{challenge.id}"
        team add teamName challenge.name
        team modify teamName prefix {"color":"gray","text":f"[{challenge.name}] "}

    forceload add 0 0

    # default score values
    unless score .latest scores.uid matches 0..:
        scores.uid['.latest'] = 0

    emitToAll("onLoad")


function vanillaTag('tick', ./tick):
    tickers.step()

    as @a[tag=!chlg.joined_before]:
        tag @s add chlg.joined_before
        scores.uid['@s'] = scores.uid['.latest']
        scores.uid['.latest'] += 1
        
        gamemode adventure @s
        tp @s @e[type=marker,tag=chlg.limbo,limit=1]
        tag @s add chlg.limbo
        tellraw @s {"color":"aqua","text":"\nWelcome to Challenge Survival!\n"}
        function ./start_selecting
        tellraw @s ""
    
    as @a[tag=chlg.limbo]:
        effect give @s resistance 1 120 true
        effect give @s regeneration 1 120 true
        effect give @s saturation 1 120 true
    
    as @a[tag=chlg.selecting]:
        title @s actionbar {"color":"aqua","text":"Use the book you received to select your challenge"}
        scoreboard players enable @s challenge_select
        scoreboard players enable @s challenge_reroll
        if score @s scores.drop matches 1.. at @s as @e[type=item,distance=..5,nbt={Age:0s,Item:{tag:{selectionBook:1b}}}]:
                data modify entity @s Owner set from entity @s Thrower
                data modify entity @s PickupDelay set value 0s
    
    as @a[scores={triggers.challenge_select=1..}]:
        scores.var['.challenge'] = triggers.challenge_select['@s']
        scoreboard players reset @s triggers.challenge_select

        for challenge in challenges:
            if score .challenge scores.var matches (challenge.id):

                unless entity @s[tag=f"chlg.can_select_{challenge.id}"]:
                    tellraw @s {"color":"red","text":f"Could not select challenge {challenge.id}. If you think this is a bug, scream at Adamanti."}
                    scoreboard players enable @s triggers.challenge_select
                
                if entity @s[tag=f"chlg.can_select_{challenge.id}"]:
                    scoreboard players reset @s triggers.challenge_reroll
                    scoreboard players enable @s triggers.challenge_view
                    tag @s remove chlg.selecting
                    tag @s remove chlg.rerolled
                    title @s actionbar ""
                    tellraw @s {"color":"aqua","text":("\nYou have selected the \"" + challenge.name + "\" challenge. Have fun :)\n")}
                    team join f"challenge{challenge.id}" @s

                    function ./clear_selection_tags
                    if score @s scores.challenge matches 1..:
                        emitToOwn("clear")

                    if entity @s[tag=chlg.limbo]:
                        function ./spawn
                    clear @s written_book{selectionBook:1b}
                    scores.challenge['@s'] = scores.var['.challenge']
                    emitToOwn("onSelect")
                    emitToOwn("onSpawn")
    
    as @a[scores={triggers.challenge_reroll=1}]:
        scoreboard players reset @s triggers.challenge_reroll
        unless entity @s[tag=chlg.selecting] run return 0
        if entity @s[tag=chlg.rerolled] run return 0
        scoreboard players enable @s triggers.challenge_select
        tellraw @s [{"color":"red","text":"\nAre you sure you want to re-roll your challenges? You will not be able to go back. "},{"text":"Click to confirm","underlined":true,"hoverEvent":{"action":"show_text","value":"Click to re-roll your challenges"},"clickEvent":{"action":"run_command","value":("/trigger " + str(triggers.challenge_reroll) + " set 2")}},{"text":"\n"}]
    
    as @a[scores={triggers.challenge_reroll=2}]:
        scoreboard players reset @s triggers.challenge_reroll
        unless entity @s[tag=chlg.selecting] run return 0
        if entity @s[tag=chlg.rerolled] run return 0
        tag @s add chlg.rerolled
        function ./start_selecting
    
    as @a[scores={triggers.challenge_view=1..}]:
        scoreboard players set @s triggers.challenge_view 0
        scoreboard players enable @s triggers.challenge_view
        for challenge in challenges:
            if entity @s[scores={scores.challenge=challenge.id}]:
                tellraw @s [{"color":"aqua","text":"Your challenge is:\n"},{"text":challenge.name,"bold":true},{"text":f": {challenge.explanation}"}]
    

    if score .airToggle scores.var matches 1..:
        as @e[scores={scores.airToggle=1..}]:
            scores.airToggle['@s'] -= 1
            Data.entity('@s')['Air'] = scores.airToggle['@s']
            if score @s scores.airToggle matches 0:
                scoreboard players reset @s scores.airToggle
                scores.var['.airToggle'] -= 1
    
    # 2 sec ticker
    if score .ticker2s scores.var matches 0:
        emitToAll("every2Sec")
    
    # 5 sec ticker
    challenges5sec = []
    for challenge in challenges:
        if challenge.overrides("every5Sec"):
            challenges5sec.append(challenge)
    for i, challenge in enumerate(challenges5sec):
        delay = floor(i / len(challenges5sec) * (20 * 5))
        if score .ticker5s scores.var matches delay:
            challenge.every5Sec()
    
    # every tick
    for challenge in challenges:
        if not challenge.overrides("everyTick"):
            continue
        challenge.everyTick()
    
    for objective, criteria in scores._criteriaDict.items():
        if criteria == "dummy":
            continue
        if objective in ("health", "respawnDeathCount"):
            continue
        score = scores[objective]
        as @a[scores={score=1..}]:
            score['@s'] = 0


function ./spawn:
    tag @s remove chlg.limbo
    clear @s written_book{selectionBook:1b}
    tp @s @e[type=marker,tag=chlg.spawn,limit=1]
    effect clear @s resistance
    effect clear @s regeneration
    effect clear @s saturation
    gamemode survival @s


function ./start_selecting:
    tag @s add chlg.selecting

    scoreboard players enable @s triggers.challenge_select
    scoreboard players enable @s triggers.challenge_reroll
    store success score .rerolled scores.var if entity @s[tag=chlg.rerolled]

    if score .rerolled scores.var matches 0:
        tellraw @s {"color":"aqua","text":"Open the book in your inventory to select your challenge.\n\nTry selecting not the most easy challenge, but the most interesting one! (otherwise you might get bored lol)"}
    if score .rerolled scores.var matches 1:
        tellraw @s {"color":"aqua","text":"You have re-rolled your challenges. Open your new book to select your challenge."}
    clear @s written_book{selectionBook:1b}

    Data.storage('challengesurvival:misc')["Pages"] = []
    dataPages = Data.storage('challengesurvival:misc')["Pages"]
    dataPages.append('{"text":"When you join this server, you can choose between 4 random challenges on the next pages of this book.\\n\\nOnce you choose, you cannot change your challenge for the rest of the game. Choose wisely!"}')
    if score .rerolled scores.var matches 0:
        dataPages.append('{"text":"If all 4 choices seem truly unfun to you, you may re-roll your 4 challenges on the last page of the book.\\n\\nYou can only re-roll once, and it is irreversible.\\n\\nBut anyway, here are your challenges:"}')

    function ./roll_4_challenges
    for challenge in challenges:
        if entity @s[tag=f"chlg.can_select_{challenge.id}"]:
            dataPages.append(
                '[{"text":"Click to select:\\n\\n"},{"text":"' + escapeDoubleQuotes(challenge.name) + '","color":"dark_aqua","underlined":true,"hoverEvent":{"action":"show_text","value":[{"text":"Click to select the \\"' + escapeDoubleQuotes(challenge.name) + '\\" challenge."}]},"clickEvent":{"action":"run_command","value":"/trigger ' + str(triggers.challenge_select) + ' set ' + str(challenge.id) + '"}},{"text":"\\n\\n\\"' + escapeDoubleQuotes(challenge.explanation) + '\\""}]'
            )

    if score .rerolled scores.var matches 0:
        dataPages.append('[{"text":"If none 4 of these challenges seem funny to you, you may:\\n\\n"},{"text":"RE-ROLL","color":"dark_red","underlined":true,"hoverEvent":{"action":"show_text","value":[{"text":"Click to reroll your challenges."}]},"clickEvent":{"action":"run_command","value":"/trigger ' + str(triggers.challenge_reroll) + ' set 1"}},{"text":"\\n\\nyour 4 choices. Beware that if you do, you can\'t go back, and you can\'t re-roll again."}]')
    if score .rerolled scores.var matches 1:
        dataPages.append('[{"text":"You have already re-rolled your challenges once. You have to pick one of the 4 challenges you rolled in this book."}]')

    at @s run summon item ~ ~ ~ {Tags:[chlg.current],Item:{id:"minecraft:written_book",Count:1b,tag:{
        selectionBook:1b,
        title:"Choose your Challenge",
        author:"Adamanti",
    }}}

    Data.storage('challengesurvival:misc')["UUID"] = Data.entity('@s')["UUID"]
    at @s as @e[type=item,tag=chlg.current]:
        tag @s remove chlg.current
        Data.entity('@s')["Owner"] = Data.storage('challengesurvival:misc')["UUID"]
        Data.entity('@s')["Item"]["tag"]["pages"] = dataPages

function ./roll_4_challenges:
    Data.storage('challengesurvival:misc')["Challenges"] = []
    for id in IDs.values():
        unless entity @a[scores={scores.challenge=id}] unless entity @a[tag=f"chlg.can_select_{id}"]:
            Data.storage('challengesurvival:misc')["Challenges"].append(id)
    store result score .challenges scores.var run data get storage challengesurvival:misc Challenges
    if score .challenges scores.var matches ..3:
        challengeList = [*IDs.values()]
        Data.storage('challengesurvival:misc')["Challenges"] = challengeList
        scores.var['.challenges'] = len(challengeList)
    function ./clear_selection_tags
    function ./roll_challenge
    function ./roll_challenge
    function ./roll_challenge
    function ./roll_challenge

function ./roll_challenge:
    store result score .challenge scores.var run random value 0..2147483646
    scores.var['.challenge'] %= scores.var['.challenges']
    Data.storage('challengesurvival:misc')["Current"] = {}
    Data.storage('challengesurvival:misc')["Current"]["index"] = scores.var['.challenge']
    with storage challengesurvival:misc Current:
        $data modify storage challengesurvival:misc Current.challenge set from storage challengesurvival:misc Challenges[$(index)]
        $data remove storage challengesurvival:misc Challenges[$(index)]
        scores.var['.challenges'] -= 1
    with storage challengesurvival:misc Current:
        $tag @s add chlg.can_select_$(challenge)

function ./clear_selection_tags:
    for id in IDs.values():
        tag @s remove f"chlg.can_select_{id}"


advancement ./respawn {
    "criteria": {
        "requirement": {
            "trigger": "minecraft:tick",
            "conditions": {
                "player": [
                    {
                        "condition": "minecraft:entity_scores",
                        "entity": "this",
                        "scores": {
                            "chlg.health": {
                                "min": 1
                            }
                        }
                    },
                    {
                        "condition": "minecraft:entity_scores",
                        "entity": "this",
                        "scores": {
                            "chlg.respawnDeathCount": {
                                "min": 1
                            }
                        }
                    }
                ]
            }
        }
    },
    "rewards": {
        "function": "challengesurvival:on_respawn"
    }
}

function ./on_respawn:
    advancement revoke @s only ./respawn
    unless entity @s[tag=chlg.dead] return run tag @s add chlg.dead

    tag @s remove chlg.dead
    scores.respawnDeathCount['@s'] = 0

    if entity @s[tag=chlg.limbo] run tp @s @e[type=marker,tag=chlg.limbo,limit=1]
    if entity @s[tag=chlg.recover] run function ./recover_items
    
    emitToOwn("onSpawn")

function ./recover_items:
    tag @s remove chlg.recover
    scores.var['.currentUid'] = scores.uid['@s']
    at @s as @e[type=item,tag=chlg.recover] if score @s scores.target = .currentUid scores.var:
        tp @s ~ ~ ~
